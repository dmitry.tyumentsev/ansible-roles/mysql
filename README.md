MySQL Role
=========

Thiss role installs [MySQL](https://www.mysql.com/) on Ubuntu/Debian server.

Example Playbook
=========
```yml
- hosts: all
  become: true
  roles:
    - mysql
```
